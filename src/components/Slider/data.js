import nail1 from './../../assets/nails/a.jpg'
import nail2 from './../../assets/nails/nails.jpg'
import nail3 from './../../assets/nails/photo_2021-02-08_18-07-32.jpg'
import nail4 from './../../assets/nails/photo_2021-02-08_18-07-35.jpg'
import nail5 from './../../assets/nails/photo_2021-02-08_18-07-45.jpg'
import nail6 from './../../assets/nails/photo_2021-02-08_18-07-52.jpg'
import nail7 from './../../assets/nails/photo_2021-02-08_18-07-54.jpg'
import nail8 from './../../assets/nails/photo_2021-02-08_18-07-40.jpg'

export let data = [
    {
        image: nail2,
        name: "Вариант 1",
        company: "Название",
        quotes:
            "Тут який дескріпшн цього стилю"
    },
    {
        image: nail1,
        name: "Вариант 2",
        company: "Название",
        quotes:
            "Тут який дескріпшн цього стилю"
    },
    {
        image: nail3,
        name: "Вариант 3",
        company: "Название",
        quotes:
            "Тут який дескріпшн цього стилю"
    },
    {
        image: nail4,
        name: "Вариант 4",
        company: "Название",
        quotes:
            "Тут який дескріпшн цього стилю"
    },
    {
        image: nail5,
        name: "Вариант 5",
        company: "Название",
        quotes:
            "Тут який дескріпшн цього стилю"
    },
    {
        image: nail6,
        name: "Вариант 6",
        company: "Название",
        quotes:
            "Тут який дескріпшн цього стилю"
    },
    {
        image: nail7,
        name: "Вариант 7",
        company: "Название",
        quotes:
            "Тут який дескріпшн цього стилю"
    },
    {
        image: nail8,
        name: "Вариант 8",
        company: "Название",
        quotes:
            "Тут який дескріпшн цього стилю"
    },
];
