import React, {useState, useEffect} from 'react'

import './intro.css'

const Intro = () => {
    const [logo, setLogo] = useState(false)
    useEffect(() => {
        window.addEventListener('load', setLogo(true));
    }, []);

    return (
        <>
            <div className="intro-container ">
                <div className="stars "/>
                <div className="luna animate__animated animate__fadeIn "/>
                <div className="twinkling"/>
                <div className="clouds animate__animated animate__backInUp"/>
                {logo ?
                    <div className={'logo wow slideInRight'}>La Luna</div>
                    : null
                }
            </div>
        </>
    )
}


export default Intro
