import React from 'react'

import IntroSection from '../Sections/intro/Intro'
import ContactSection from '../Sections/contact-section/ContactSection'
import DisclaimerSection from '../Sections/disclaimer/Disclaimer'
import FooterSection from '../Sections/footer/Footer'

import './../App/App.scss'
import Map from "./../map/Map";
import Basic from "../Slider";


const location = {
    address: '1600 Amphitheatre Parkway, Mountain View, california.',
    lat: 50.43275829324964,
    lng: 30.321494511502227,
} // our location object from earlier


function App() {
    return (
        <div className="App">
            <IntroSection/>
            <Basic/>

            <ContactSection/>
            <DisclaimerSection/>
            <Map location={location} zoomLevel={17}/>
            <FooterSection/>
        </div>
    )
}

export default App
